//EXPRESS
const express = require('express')
const app = express()
const port = process.env.port || 3000;

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

// SQLite
const sqlite3 = require('sqlite3').verbose();

//OPEN DATABASE
let db = new sqlite3.Database('./db/blogfutur.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err)
    {
        return console.error(err.message);
    }
    console.log('Connecté a la BDD Blog du futur');
});

// set the view engine to ejs
app.set('view engine', 'ejs');
//for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

/////ROUTES/////

//INDEX
app.get('/', (req, res) => {
    const query = `SELECT * FROM articles`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        res.render('pages/index', {
            articles: rows
        });
    });
});

//PAGE DES ARTICLES
app.get('/articles', (req, res) => {
    const query = `SELECT * FROM articles`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        res.render('pages/articles', {
            articles: rows
        });
    });
});

//PAGE ARTICLE
app.get('/article/:id', (req, res) => {
    const query = `SELECT * FROM articles WHERE id = ${req.params.id}`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        res.render('pages/article', {
            articles: rows
        });
    });
});

//PAGE WORKSPACE
app.get('/workspace', (req, res) => {
    renderWorkspace(res)
});

app.get('/workspace/newArticle', (req, res) => {
    resetFormData();
    renderWorkspace(res, true);
});

//PAGE ABOUT
app.get('/about', (req, res) => {
    res.render('pages/about');
});
/////CRUD/////

//create article

app.post('/workspace/create', (req, res) => {
    const query = `INSERT INTO article (titre, thumbnail_url, texte, auteur, date)
        VALUES ('${req.body.inputTitre}', 
            '${req.body.inputThumbNail}',
            '${req.body.inputTexte}',
            ${req.body.inputAuteur},
            ${req.body.inputDate}
        )`;

    db.run(query, (err) => {
        if (err)
        {
            return console.error('ici: ', err.message);
        }
        console.log('Article successfully created.');

        renderWorkspace(res);
    });
});


//read article

app.get('/workspace/:id', (req, res) => {
    if (isNaN(req.params.id))
    {
        return;
    }

    const query = `SELECT * FROM article WHERE id = ${req.params.id}`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        if (!rows)
        {
            renderWorkspace(res);
            return;
        }

        const data = rows[0];

        formData = {
            id: data.id,
            titre: data.titre,
            thumbnail_url: data.thumbnail_url,
            texte: data.texte,
            auteur: data.auteur,
            date: data.date
        }

        res.render('pages/workspace', {
            article: rows,
            formData: formData,
            showCreateArticle: false,
            showUpdateArticle: true
        });
    });
});


//update product

app.post('/workspace/update/:id', (req, res) => {
    const query = `UPDATE article
        SET titre = '${req.body.inputTitre}',
        thumbnail_url = '${req.body.inputThumbNail}',
        texte = '${req.body.inputTexte}',
        auteur = ${req.body.inputAuteur},
        date = ${req.body.inputDate}
        WHERE id = ${req.params.id}
        `;

    db.run(query, (err) => {
        if (err)
        {
            return console.error(err.message);
        }
        console.log('Article successfully updated.');

        resetFormData();

        renderWorkspace(res);
    });
});


//delete product

app.get('/workspace/delete/:id', (req, res) => {
    if (isNaN(req.params.id))
    {
        return;
    }

    const query = `DELETE FROM article WHERE id = ${req.params.id};`;

    db.run(query, (err) => {
        if (err)
        {
            return console.error(err.message);
        }
        console.log('Article successfully deleted.');

        renderWorkspace(res);
    });
});

/////UTILITIES/////

function renderWorkspace(res, showCreateArticle = false) {
    resetFormData();

    if (showCreateArticle)
    {
        res.render('pages/workspace', {
            article: [],
            formData: formData,
            showCreateArticle: showCreateArticle,
            showUpdateArticle: false
        });

        return;
    }

    const query = `SELECT * FROM article`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error('here: ', err.message);
        }

        res.render('pages/workspace', {
            article: rows,
            formData: formData,
            showCreateArticle: showCreateArticle,
            showUpdateArticle: false
        });
    });
}

function resetFormData() {
    formData = {
        id: null,
        titre: '',
        thumbnail_url: '',
        texte: '',
        auteur: '',
        date:''
    };
}

